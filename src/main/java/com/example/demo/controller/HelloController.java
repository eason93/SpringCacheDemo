package com.example.demo.controller;

import com.example.demo.dto.Person;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {
    @GetMapping("/person")
    public Person Person(){
        Person person=new Person();
        person.setName("zrq");
        return person;
    }
}
