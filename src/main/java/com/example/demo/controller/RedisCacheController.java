package com.example.demo.controller;

import com.example.demo.dto.Person;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/rest")
public class RedisCacheController {
        @GetMapping("/get")
//        @Cacheable(key="#person.name")
        @Cacheable(value="models",key="#person.name")
        public String getPerson(Person person){
            System.out.println("Person.address:"+person.getAddress());
            return person.getAddress();
        }

        //删除cache
        @DeleteMapping("/delete/{name}")
        @CacheEvict(value="models",key="#name")
        public String deleteCache(@PathVariable("name") String name){
            System.out.println("system input name:"+name);
            return "delete Cache success!";
        }

        @PutMapping("/put")
        @CachePut(value="models",key="#person.name")
        public String putCache(Person person){
            System.out.println("key:"+person.getName()+","+"address:"+person.getAddress());
            return person.getAddress();
        }

}
