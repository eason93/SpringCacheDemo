package com.example.demo.SchedulerTask;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class SpringCacheScheduler {

/*
    scheduled单位是ms
    10S清理一下缓存
     */
    @Scheduled(fixedRate=10000)
    @CacheEvict(value="models", allEntries = true)
    public void cacheRemove(){
    }
}
